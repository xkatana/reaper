var mongoose = require('mongoose'),
    EmployeeAccount = mongoose.model("Account"),
    Question = mongoose.model("Question"),
    ObjectId = mongoose.Types.ObjectId;

var Infinity = 100000;
var openConnections = [];
/**
 *  register conference by an employee basic information an WeChat openId
 * @param req
 * @param res
 * @param next
 */
exports.attendConference = function (req, res, next) {
    var employeeAccountModel = new EmployeeAccount(req.body);
    employeeAccountModel.save(function (err, employee) {
        if (err) {
            res.status(500);
            res.json({
                type: false,
                data: "Error occured: " + err
            })
        } else {
            res.json({
                type: true,
                data: employee
            })
        }
    })
    //EmployeeAccount.findOne({employeeId: employeeAccountModel.employeeId}, function (err, employee) {
    //    if (err) {
    //        res.json({
    //            type: false,
    //            data: "Error occured: " + err
    //        });
    //    } else {
    //        if (employee) {
    //            employee.save(function (err, result) {
    //                res.json({
    //                    type: true,
    //                    data: "employee: " + employeeAccountModel.employeeId + " attended the conference !"
    //                });
    //            });
    //        } else {
    //            res.status(404);
    //            res.json({
    //                type: false,
    //                data: "employee: " + employeeAccountModel.employeeId + " not found"
    //            });
    //        }
    //    }
    //})
}
exports.askQuestion = function (req, res, next) {
    var questionModel = new Question(req.body);
    questionModel.save(function (err, question) {
        if (err) {
            res.status(500);
            res.json({
                type: false,
                data: "Error occured: " + err
            })
        } else {
            openConnections.forEach(function(resp) {
                var d = new Date();
                resp.write('id: ' + d.getMilliseconds() + '\n');
                resp.write('data:' + JSON.stringify(question) +   '\n\n'); // Note the extra newline
            });
            res.json({
                type: true,
                data: question
            })
        }
    })
}

exports.noticeMessage = function (req, res, next) {
    // set timeout as high as possible
    req.socket.setTimeout(Infinity);
    // send headers for event-stream connection
    // see spec for more information
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive'
    });
    res.write('\n');
    // push this res object to our global variable
    openConnections.push(res);
    // When the request is closed, e.g. the browser window
    // is closed. We search through the open connections
    // array and remove this connection.
    req.on("close", function () {
        var toRemove;
        for (var j = 0; j < openConnections.length; j++) {
            if (openConnections[j] == res) {
                toRemove = j;
                break;
            }
        }
        openConnections.splice(j, 1);
        console.log(openConnections.length);
    });
}

/**
 *  create a new employee account
 * @param req
 * @param res
 * @param next
 */
exports.createEmployeeAccount = function (req, res, next) {
    var employeeAccountModel = new EmployeeAccount(req.body);
    employeeAccountModel.save(function (err, employee) {
        if (err) {
            res.status(500);
            res.json({
                type: false,
                data: "Error occured: " + err
            })
        } else {
            res.json({
                type: true,
                data: employee
            })
        }
    })
}
